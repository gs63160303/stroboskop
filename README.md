# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://gs63160303@bitbucket.org/gs63160303/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/gs63160303/stroboskop/commits/0307e595c068c5624ff4c2acb9703628e6570a5f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/gs63160303/stroboskop/commits/f46fe44f4be75b955b1743a49062c125f57cb42b

Naloga 6.3.2:
https://bitbucket.org/gs63160303/stroboskop/commits/66295d6e74252c0d9c02150d7a121508b79aa333

Naloga 6.3.3:
https://bitbucket.org/gs63160303/stroboskop/commits/dfff0dff3311755816881aea98439e954150fc9d

Naloga 6.3.4:
https://bitbucket.org/gs63160303/stroboskop/commits/3440d00161a96ee6ffffec63c3c037dbef0e4966

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/gs63160303/stroboskop/commits/d8f8e3b12883752989a5ee1df898caa7300c1f56

Naloga 6.4.2:
https://bitbucket.org/gs63160303/stroboskop/commits/41318343de2656702e5719682d901f19a878d5a7

Naloga 6.4.3:
https://bitbucket.org/gs63160303/stroboskop/commits/8f255f3b6760247d7b3348a8ef41672e9ac0f04f

Naloga 6.4.4:
https://bitbucket.org/gs63160303/stroboskop/commits/e1c01cd43bf4d79ac85eefc446227371b2b96bb2